const config = {
    companyName: 'Some Beautifully Company',
    user: {
        name: 'Петр',
        surname: 'Нетипичный',
        avatar: 'Avatar.png',
        balance: 100500
    },
    users: [
        {
            name: 'Григорий',
            surname: 'Античный',
            avatar: 'Avatar.png',
            balance: 500,
            phone: '89999999999'
        },
        {
            name: 'Мария',
            surname: 'Святая',
            avatar: 'Avatar.png',
            balance: 55500,
            phone: '89999999998'
        },
        {
            name: 'Дмитрий',
            surname: 'Важный',
            avatar: 'Avatar.png',
            balance: 87500,
            phone: '89999999997'
        }
    ],
    transfersHistory: [
        {
            phone: '89999999999',
            date: '15.04.2023',
            status: 'incoming',
            count: 500
        },
        {
            phone: '89999999998',
            date: '11.04.2023',
            status: 'incoming',
            count: 1500
        },
        {
            phone: '89999999997',
            date: '13.05.2023',
            status: 'incoming',
            count: 3500
        },
        {
            phone: '89999999996',
            date: '15.04.2023',
            status: 'outgoing',
            count: 15000
        },
        {
            phone: '89999999995',
            date: '15.04.2023',
            status: 'outgoing',
            count: 1000
        }
    ],
    friends: [
        '89999999999',
        '89999999998',
        '89999999997',
    ]
}

export { config }
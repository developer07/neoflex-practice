import './style.scss'
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";
import moment from "moment";
import {useRef} from "react";
import {Toast} from "primereact/toast";

const History = ({config, addTransfer}) => {
    const tableData = config.transfersHistory.map(th => {
        const user = config.users.find(u => u.phone === th.phone)
        if (user) {
            return {
                ...th,
                fullname: user.surname + " " + user.name
            }
        }
    }).filter(td => td).sort((a,b) => moment(a.date, 'DD.MM.YYYY') > moment(b.date, 'DD.MM.YYYY') ? -1 : 1)

    const statusBodyTemplate = (rowData) => {
        return rowData.status === 'outgoing' ? <div
            className={'repeat'}
            onClick={() => {
                if(rowData.count <= config.user.balance){
                    addTransfer(rowData.phone, rowData.count)
                    toastRef.current.show({severity: 'success', message: 'Готово', detail: 'Перевод выполнен'})
                } else toastRef.current.show({severity: 'error', message: 'Ошибка', detail: 'Недостаточно средств'})
            }}
        >
            Повторить
        </div> : null;
    }

    const countBodyTemplate = (rowData) => {
        return <div style={{color: rowData.status === 'incoming' ? 'lightgreen' : 'red'}}>{rowData.count}</div>
    }

    const toastRef = useRef()

    return [
        <Toast ref={toastRef} position='bottom-right' />,
        <DataTable value={tableData} responsiveLayout="scroll">
            <Column field="fullname" header="ФИО"></Column>
            <Column field="date" header="Дата"></Column>
            <Column field="count" header="Сумма" body={countBodyTemplate}></Column>
            <Column field="status" header="Действие" body={statusBodyTemplate}></Column>
        </DataTable>
    ]
}

export default History

import './style.scss'
import Logout from '../../assets/img/Logout.svg'

const Top = ({config}) => {
    return <div className={'top'}>
        <div className={'company-name'}>{config.companyName}</div>
        <div className={'user'}>
            <div className={'username'}>{config.user.surname} {config.user.name}</div>
            <img src={require(`../../assets/img/${config.user.avatar}`)}/>
            <img src={Logout}/>
        </div>
    </div>
}

export default Top
import './style.scss'
import {useState} from "react";
import {useHistory} from "react-router-dom";

const SideBlock = ({config}) => {
    const [activeTab, setActiveTab] = useState(0)
    const events = [
        {
            label: 'Перевести',
            path: '/'
        },
        {
            label: 'История операций',
            path: '/history'
        },
        {
            label: 'Список друзей',
            path: '/friends'
        }
    ]
    const history = useHistory()

    return <div className={'side-block'}>
        <div className={'balance'}>
            <div className={'count'}>
                {config.user.balance}
            </div>
            <div className={'title'}>
                Остаток средств
            </div>
        </div>
        <div className={'events'}>
            {events.map((e, ind) =>
                <div
                    className={ind === activeTab ? 'active' : ''}
                    onClick={() => {
                        setActiveTab(ind)
                        history.push(e.path)
                    }}
                >
                    {e.label}
                </div>
            )}
        </div>
    </div>
}

export default SideBlock
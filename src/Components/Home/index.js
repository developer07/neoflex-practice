import './style.scss'
import {useRef, useState} from "react";
import {Dropdown} from "primereact/dropdown";
import {Button} from "primereact/button"
import {InputText} from "primereact/inputtext";
import {Toast} from "primereact/toast";

const Home = ({config, addTransfer}) => {
    const [transfer, setTransfer] = useState({
        recipient: '',
        count: 0
    })

    const toastRef = useRef()

    return <div className={'home'}>
        <Toast ref={toastRef} position='bottom-right' />
        <div className={'input-group'}>
            <div>Получатель</div>
            <Dropdown
                value={transfer.recipient}
                options={config.friends}
                onChange={(e) =>
                    setTransfer({...transfer, recipient: e.value}
                )}
                editable
                placeholder="Выберите из списка или введите номер"
            />
        </div>
        <div className={'input-group'}>
            <div>Сумма</div>
            <InputText
                value={transfer.count}
                onChange={(e) => {
                    if (e.target.value <= config.user.balance) {
                        setTransfer({...transfer, count: e.target.value})
                    } else setTransfer({...transfer, count: config.user.balance})
                }}
            />
        </div>
        <div className={'buttons-group'}>
            <Button
                label="Отмена"
                className="p-button-outlined p-button-secondary"
                onClick={() => {
                    setTransfer({recipient: '', count: 0})
                }}
            />
            <Button
                label="Перевести"
                className="p-button-raised"
                disabled={!transfer.recipient || !transfer.count}
                onClick={() => {
                    if (config.users.find(u => u.phone === transfer.recipient)){
                        addTransfer(transfer.recipient, transfer.count)
                        toastRef.current.show({severity: 'success', message: 'Готово', detail: 'Перевод выполнен'})
                        setTransfer({recipient: '', count: 0})
                    } else toastRef.current.show({severity: 'error', message: 'Ошибка', detail: 'Пользователь не найден'})
                }}
            />
        </div>
    </div>
}

export default Home
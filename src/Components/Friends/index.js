import './style.scss'
import {Column} from "primereact/column";
import {DataTable} from "primereact/datatable";
import {Button} from "primereact/button";

const Friends = ({config, deleteFriend}) => {
    const dataTable = config.friends.map(f => {
        return config.users.find(u => u.phone === f)
    })

    const deleteBodyTemplate = (rowData) => {
        return <Button
            label="Удалить"
            className="p-button-outlined p-button-warning"
            onClick={() => {
                deleteFriend(rowData.phone)
            }}
        />
    }

    return <DataTable value={dataTable} responsiveLayout="scroll">
        <Column field="surname" header="Фамилия"></Column>
        <Column field="name" header="Имя"></Column>
        <Column field="phone" header="Номер"></Column>
        <Column header="" body={deleteBodyTemplate}></Column>
    </DataTable>
}

export default Friends
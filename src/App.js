import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Home from "./Components/Home";
import History from "./Components/History";
import Friends from "./Components/Friends";
import Top from "./Components/Top";
import { config } from "./config";
import SideBlock from "./Components/SideBlock";
import {useState} from "react";
import moment from 'moment'


import 'primereact/resources/themes/lara-light-indigo/theme.css'
import "primereact/resources/primereact.min.css"
import 'primeicons/primeicons.css'

import './app.scss'

function App() {
    const [data, setData] = useState(config)

    const addTransfer = (recipient, count) => {
        const transfer = {
            phone: recipient,
            date: moment().format('DD.MM.YYYY'),
            status: 'outgoing',
            count: count
        }
        let newTransfers = data.transfersHistory
        newTransfers.push(transfer)
        setData({
            ...data,
            user: {...data.user, balance: data.user.balance - count},
            transfersHistory: newTransfers
        })
    }

    const deleteFriend = (phone) => {
      const newFriends = data.friends.filter(f => f !== phone)
        setData({
            ...data,
            friends: newFriends
        })
    }

    return <Router>
        <Top config={data}/>
        <div className={'layout'}>
            <SideBlock config={data}/>
            <div className={'content'}>
                <Switch>
                    <Route exact path="/">
                        <Home config={data} addTransfer={addTransfer}/>
                    </Route>
                    <Route exact path="/history">
                        <History config={data} addTransfer={addTransfer}/>
                    </Route>
                    <Route exact path="/friends">
                        <Friends config={data} deleteFriend={deleteFriend}/>
                    </Route>
                </Switch>
            </div>
        </div>
    </Router>
}

export default App;
